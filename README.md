# COVID-19 - Mask Detector

# Credits

This code was mostly from https://github.com/JadHADDAD92/covid-mask-detector

# Reproduction
```Shell

git clone git@gitlab.com:maxence.de-bigault-casanove/mask-detection-for-coronavirus.git 
or
git clone https://gitlab.com/maxence.de-bigault-casanove/mask-detection-for-coronavirus.git


# Create a virtual environment and activate it.

pip install -r requirements.txt

# Download dataset and export it to pandas DataFrame
python -m covid-mask-detector.data_preparation
```
## Training

```Shell
python -m covid-mask-detector.train
```

## Testing on videos
```sh
python -m covid-mask-detector.data_preparation.video modelPath videoPath
```

### Usage
```
Usage: video.py [OPTIONS] MODELPATH VIDEOPATH

  modelPath: path to model.ckpt

  videoPath: path to video file to annotate

Options:
  --output PATH  specify output path to save video with annotations
```

